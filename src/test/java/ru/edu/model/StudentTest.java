package ru.edu.model;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static ru.edu.StudentsRepositoryCRUDTest.students;

public class StudentTest {

    @Test
    public void testEquals_Symmetric() {

        Student x = new Student(
                students[0].getFirstName(),
                students[0].getLastName(),
                students[0].getBirthDate(),
                students[0].isGraduated());

        Student y = new Student(
                students[0].getFirstName(),
                students[0].getLastName(),
                students[0].getBirthDate(),
                students[0].isGraduated());

        assertTrue(x.equals(y) && y.equals(x));
        assertTrue(x.hashCode() == y.hashCode());

        assertTrue(x.equals(x));

        assertFalse(x.equals(new Object()));

        assertFalse(students[0].equals(students[1]));
        assertFalse(students[0].equals(students[2]));
        assertFalse(students[1].equals(students[2]));

    }

}