package ru.edu;

import org.junit.Test;
import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static ru.edu.StudentsRepositoryCRUDTest.students;

public class JdbcTest {

    public static final String JDBC_H2_LOCAL_DB = "jdbc:h2:./target/localDb/db";
    public static final String H2_LOCAL_DB_USER = "sa";
    public static final String H2_LOCAL_DB_PASSWORD = "";

    @Test
    public void testJdbc() throws SQLException {

        Connection connection = getConnection();
        connection.close();

    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(JDBC_H2_LOCAL_DB , H2_LOCAL_DB_USER , H2_LOCAL_DB_PASSWORD);
    }


    @Test
    public void testSelectFromEmptyTable() throws SQLException {

        try (Connection connection = getConnection()) {

            StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(connection);
            crud.removeAll();

            assertNull(crud.selectAll());
            assertNull(crud.selectById(UUID.randomUUID()));
            assertEquals(0, crud.remove(UUID.randomUUID()));
            assertEquals(0, crud.removeAll());

        }
    }

    @Test(expected = IllegalStateException.class)
    public void testUpdateStudentWithWrongId () throws SQLException {

        testSelectFromEmptyTable();

        try (Connection connection = getConnection()) {

            StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(connection);

            Student notExistedStudent = new Student(
                    students[0].getFirstName(),
                    students[0].getLastName(),
                    students[0].getBirthDate(),
                    students[0].isGraduated());
            notExistedStudent.setId(UUID.randomUUID());

            crud.update(notExistedStudent);

        }

    }

}
