package ru.edu;

import org.junit.Test;
import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static org.junit.Assert.*;
import static ru.edu.JdbcTest.getConnection;

public class StudentsRepositoryCRUDTest {

    UUID uuid;

    public static Student[] students = {
            new Student("Danila",
                    "Bogrov",
                    new GregorianCalendar(1975, Calendar.AUGUST, 1).getTime(),
                    true),
            new Student(
                    "Ivan",
                    "Petrov",
                    new GregorianCalendar(1979, Calendar.OCTOBER, 23).getTime(),
                    false),
            new Student(
                    "Petr",
                    "Ivanov",
                    new GregorianCalendar(1981, Calendar.JANUARY, 1).getTime(),
                    true)};



    @Test(expected = IllegalStateException.class)
    public void crudTest() throws SQLException {

        try (Connection connection = getConnection()) {

            StudentsRepositoryCRUDImpl crud = new StudentsRepositoryCRUDImpl(connection);
            crud.removeAll();

            for ( int i = 0 ; i < students.length; i++ ) {
                assertNull(students[i].getId());
                uuid = crud.create(students[i]);
                assertNotNull(uuid);
                assertNotNull(students[i].getId());
            }
            System.out.println("Created students: " + Arrays.toString(students));
            System.out.println();


            Student newStudent = crud.selectById(students[0].getId());
            assertEquals(students[0].getFirstName(), newStudent.getFirstName());
            assertEquals(students[0].getLastName(), newStudent.getLastName());
            assertEquals(students[0].getBirthDate(), newStudent.getBirthDate());
            assertEquals(students[0].isGraduated(), newStudent.isGraduated());

            System.out.println("Selected student:"  + newStudent);
            System.out.println();
            System.out.println("Selected student:"  + newStudent);
            System.out.println();

            List<Student> studentsFromDB = crud.selectAll();
            assertEquals(students.length, studentsFromDB.size());
            System.out.println("List of all students: " + studentsFromDB);
            System.out.println();

            newStudent.setFirstName(students[students.length - 1].getFirstName());
            newStudent.setLastName(students[students.length - 1].getLastName());
            newStudent.setBirthDate(students[students.length - 1].getBirthDate());
            newStudent.setGraduated(students[students.length - 1].isGraduated());

            crud.update(newStudent);
            System.out.println("Selected student was updated: " + newStudent);

            assertEquals(newStudent, students[students.length - 1]);

            System.out.println("New list of students: " + crud.selectAll());
            System.out.println();


            UUID[] uuids = {students[0].getId(),
                    students[1].getId()};

            int result = crud.remove(new ArrayList<>(Arrays.asList(uuids)));
            assertEquals(uuids.length, result);
            List<Student> newList = crud.selectAll();
            assertEquals((students.length - uuids.length), newList.size());

            System.out.println("New list of students after removing: " + newList);

            crud.create(students[0]);

        }

    }

}