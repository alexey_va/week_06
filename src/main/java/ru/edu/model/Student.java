package ru.edu.model;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * Класс, отражающий структуру хранимых в таблице со студентами полей.
 */
public class Student {

    /**
     * Первичный ключ.
     * <p>
     * Рекомендуется генерировать его только внутри
     * StudentsRepositoryCRUD.create(),
     * иными словами до момента пока объект не будет сохранен в БД,
     * он не должен иметь значение id.
     */
    private UUID id;

    /**
     * First name.
     */
    private String firstName;

    /**
     * Last name.
     */
    private String lastName;

    /**
     * Birthdate.
     */
    private Date birthDate;

    /**
     * Is student graduated?
     */
    private boolean isGraduated;

     /**
     * Student constructor.
     * @param fName - First Name
     * @param lName - Last Name
     * @param bDate - Birth Date
     * @param isGrad - Student is graduated?
     */
    public Student(final String fName,
                   final String lName,
                   final Date bDate,
                   final boolean isGrad) {

        this.firstName = fName;
        this.lastName = lName;
        this.birthDate = bDate;
        this.isGraduated = isGrad;

    }

    /**
     * Empty constructor.
     */
    public Student() {

    }

    /**
     * Student ID.
     * @return UUID with ID
     */
    public UUID getId() {
        return id;
    }

    /**
     * Set ID.
     * @param studentId Student UUID
     */
    public void setId(final UUID studentId) {
        this.id = studentId;
    }

    /**
     * Get First Name.
     * @return String with First Name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set First Name.
     * @param fName Student first name
     */
    public void setFirstName(final String fName) {
        this.firstName = fName;
    }

    /**
     * Get Last Name.
     * @return String with Last Name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set Last Name.
     * @param lName Student last name
     */
    public void setLastName(final String lName) {
        this.lastName = lName;
    }

    /**
     * Get Birth Date.
     * @return Date object with Birth Date
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Set Birthdate.
     * @param bDate Student birthdate
     */
    public void setBirthDate(final Date bDate) {
        this.birthDate = bDate;
    }

    /**
     * Get "Is student graduated?" info.
     * @return Boolean
     */
    public boolean isGraduated() {
        return isGraduated;
    }

    /**
     * Set "Is student graduated?" info.
     * @param graduated "Is student graduated?" boolean field
     */
    public void setGraduated(final boolean graduated) {
        isGraduated = graduated;
    }

    /**
     * To String method.
     * @return Student's info.
     */
    @Override
    public String toString() {
        return "\nStudent{"
                + "id=" + id
                + ", firstName='" + firstName + '\''
                + ", lastName='" + lastName + '\''
                + ", birthDate=" + birthDate
                + ", isGraduated=" + isGraduated
                + '}';
    }

    /**
     * Student equals method.
     * @param o Student object
     * @return Are students equals?
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Student)) {
            return false;
        }
        Student student = (Student) o;
        return isGraduated() == student.isGraduated()
                && getFirstName().equals(student.getFirstName())
                && getLastName().equals(student.getLastName())
                && getBirthDate().equals(student.getBirthDate());
    }

    /**
     * Student's hashCode method.
     * @return Student's hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(),
                getLastName(),
                getBirthDate(),
                isGraduated());
    }
}
